import slick from 'slick-carousel';

$(function () {
  // PROMO SLIDER
  $('.js-slider').slick({
    infinite: true,
    arrows: true,
    dots: true
  });
})