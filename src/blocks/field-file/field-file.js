$(function(){
  //attached file name print
  $('.js-file-input').change(function(e){
    const $parent = $(this).parents('.js-file');
    $parent.find('.js-file-print').html(e.target.files[0].name);
  });
})