import slick from 'slick-carousel';

$(function () {
  // PRIMARY SLIDER

  $('.js-slider-wrapper').each(function() {
    const $slider = $(this).find('.js-slider');
    $slider.slick({
      infinite: true,
      arrows: true,
      adaptiveHeight: true,
      dots: true,
      prevArrow: '<button type="button" class="slick-prev"></button>',
      nextArrow: '<button type="button" class="slick-next"></button>',
      appendDots: $(this).find('.js-slider-dots'),
      appendArrows: $(this).find('.js-slider-arrows'),
    });
  });
})