import $ from 'jquery';
import select2 from 'select2';
import magnificPopup from 'magnific-popup';

import '@root/layout/header/header';
import '@root/blocks/slider/slider';
import '@root/blocks/field-file/field-file';
import './tab';

$(document).ready(function () {
  //custom select styling
  $('.js-select').select2({
    minimumResultsForSearch: -1
  });

  // tabs
  $('.js-tabs .tab-link').click(function (e) {
    e.preventDefault();
    var id = $(this).attr('href');
    var wrap = $(this).parents('.js-tabs');
    wrap.find('.tab-link').removeClass('active');
    $(this).addClass('active');
    wrap.find('.tab-block').removeClass('active');
    $(id).addClass('active');
  });

  //youtube player
  $('.js-popup-youtube').magnificPopup({
    disableOn: 700,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,
    fixedContentPos: false
  });

  //popup
  $('.js-popup').magnificPopup({
    type: 'inline',

    fixedContentPos: false,
    fixedBgPos: true,

    overflowY: 'auto',

    closeBtnInside: true,
    preloader: false,

    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-zoom-in'
  });

});