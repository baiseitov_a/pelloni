import '@root/scss/contacts.scss';
import '@root/js/common';

$(function(){

  if (typeof ymaps != 'undefined') {
    if ($('#map').length){
      const  __map = $('#map');
      const mLat = parseFloat(__map.attr('data-geo-lat'));
      const mLng = parseFloat(__map.attr('data-geo-lng'));
      ymaps.ready(function () {
          const myMap = new ymaps.Map('map', {
                  center: [mLat, mLng],
                  zoom: 16
              }, {
                  searchControlProvider: 'yandex#search'
              });
          const myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                hintContent: '',
                balloonContent: ''
            })

        myMap.geoObjects
            .add(myPlacemark);
          myMap.behaviors.disable('scrollZoom');
      }); 
    }
  }

})